FROM python:3.11

RUN apt-get update && apt-get install -y gettext netcat-traditional

WORKDIR /app/
ADD requirements.txt /app/
RUN pip install -r requirements.txt

ADD ./app /app/

RUN python manage.py collectstatic --noinput

COPY ./app/entrypoint.sh entrypoint.sh