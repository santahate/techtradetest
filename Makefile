.PHONY: up start
up start:
	docker-compose up -d

.PHONY: build
build:
	docker-compose build

.PHONY: down stop
down stop:
	docker-compose down
