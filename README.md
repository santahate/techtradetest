# TechTrade test task
This is a test task for TechTrade company.

<details>
  <summary>The task:</summary>

## Мета завдання
Метою цього завдання є розробка сервісу для отримання, обробки та візуалізації даних з біржі Binance. Основною метою є побудова власного Order Book на основі живих та історичних даних, їх об'єднання по часу та створення дашборду для перегляду в реальному часі. Додатковою метою є демонстрація ваших здібностей у виборі технологій, архітектурних рішень та організації проєкту.

## Вимоги
1. Сервіс для отримання даних з Binance:
   Розробіть Python-сервіс, який отримує дані з біржі Binance (вибір технологій залишається за вами).
2. Побудова Order Book:
   Реалізуйте функціональність для побудови та оновлення Order Book на основі отриманих даних.
3. Зберігання даних:
   Забезпечте об'єднання даних по часу та їх зберігання у вибраному сховищі (наприклад, база даних).
4. Дашборд для візуалізації:
   Створіть мінімалістичний дашборд для відображення даних з Order Book в реальному часі (вибір технологій залишається за вами).
5. Докеризація:
   Докеризуйте свій проєкт для забезпечення легкості розгортання та управління.
6. Makefile:
   Реалізуйте Makefile для збирання, запуску та зупинки сервісу.

## Додаткові вказівки
- Вибір технологій, архітектурних рішень та бібліотек залишається за вами. Вам слід обґрунтувати свій вибір у документації.
- Зверніть увагу на організацію коду, читабельність та структуру проєкту.
- Після завершення розробки, створіть документацію, яка пояснює, як запускати проєкт, як користуватись дашбордом та як розширювати функціональність.
- Оформіть ваш проєкт у гіт-репозиторії та поділіться посиланням на репозиторій разом зі звітом про виконане завдання(вітається розробка з історією комітів)
- Завдання на 1 робочий день і потрібно зробити все, що встигнете і, як бачите. Оцінюватись буде функціональність, повнота, якість(коду, архітектури, документації), глибина й об'ємність отриманого ордербуку(це важливо) і швидкість роботи(летенсі)
</details>

## Getting Started
To get a local copy up and running follow these simple steps.

### Prerequisites
Before you begin, ensure you have met the following requirements:
* You have installed the latest version of [Docker](https://www.docker.com/get-started) and [Docker Compose](https://docs.docker.com/compose/install/).
* You have a basic understanding of Docker containerization and Docker Compose.
* You have installed [Git](https://git-scm.com/downloads) for cloning the repository.

### Installation
1. Clone the repository:
   ```bash
   git clone https://gitlab.com/santahate/techtradetest.git
   cd techtradetest
   ```
   
2. Create a `.env` file from the provided template:
   ```bash
   cp .env.example .env
   ```

3. Start the containers:
   ```bash
   make start
   ```

4. Open the dashboard in your browser:
   ```
    http://localhost
    ```
   
5. Stop the containers:
    ```bash
    make stop
    ```

## Deployment
This system is intended for demonstration and development purposes only and is not intended for production use. It is designed with a basic configuration that likely does not meet the security and performance standards required for a production environment.

If you consider using this system in a production setting, it would require significant modifications, including, but not limited to:

- Implementing proper security measures such as secure communication (SSL/TLS), secure storage of sensitive data, and regular security audits.
- Configuring a more robust database solution with backups, replication, and failover strategies.
- Setting up a scalable infrastructure that can handle production-level load and traffic.
- Ensuring compliance with relevant laws and regulations regarding data protection and privacy.

Please exercise caution and seek professional advice when making the necessary adjustments for any production deployment.

## Extending Functionality
The system is designed to be easily extensible. You can add new functionality by creating new services and connecting them to the existing ones.

### Adding New Tasks

If you're adding new periodic tasks, such as for data processing or maintenance operations, follow these steps:

- Define a new task function in the `tasks.py` file.
- Set task route for new task in `celery.py` - use existing worker or deploy new one.
- Update the `Command` class in `management/commands` if necessary to include the setup for the new task.


## License
This project uses the following license: [cc-by-nc-sa-4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
