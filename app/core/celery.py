from celery import Celery

app = Celery('app')
app.config_from_object('django.conf:settings')

app.conf.task_routes = {
    'order_book.tasks.UpdateOrderBook': {
        'default': 'background_tasks',
        'priority': 10,
    },
    'order_book.tasks.ClearOldOrderBook': {
        'default': 'background_tasks',
        'priority': 10,
    },
}

app.autodiscover_tasks()
