from django.conf import settings as django_settings


def settings(request):
    """
    Add the settings to the context.
    :param request:
    :return:
    """
    return {
        'ORDER_BOOK_UPDATE_INTERVAL': django_settings.ORDER_BOOK_UPDATE_INTERVAL,
    }
