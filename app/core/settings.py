import os
from pathlib import Path

import redis

BASE_DIR = Path(__file__).resolve().parent.parent

SECRET_KEY = os.environ.get('APP_SECRET_KEY', '')

DEBUG = os.environ.get('APP_DEBUG', True)

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_celery_beat',
    'order_book',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'core.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                'core.context_processors.settings',
            ],
        },
    },
]

WSGI_APPLICATION = 'core.wsgi.application'


MAIN_DB_HOST = os.environ.get('APP_MYSQL_HOST', '127.0.0.1')
MAIN_DB_PORT = os.environ.get('APP_MYSQL_PORT', '3306')
MAIN_DB_USER = os.environ.get('APP_MYSQL_USER', 'mysql_user')
MAIN_DB_PASS = os.environ.get('APP_MYSQL_PASS', 'mysql_pass')
MAIN_DB_NAME = os.environ.get('APP_MYSQL_NAME', 'trade')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': MAIN_DB_NAME,
        'USER': MAIN_DB_USER,
        'PASSWORD': MAIN_DB_PASS,
        'HOST': MAIN_DB_HOST,
        'PORT': MAIN_DB_PORT,
        'OPTIONS': {
          'autocommit': True,
          'charset': 'utf8mb4',
          'use_unicode': True,
        },
        'TEST': {
            'NAME': 'test_%s' % MAIN_DB_NAME,
        },
    },
}

REDIS_HOST = os.environ.get('APP_REDIS_HOST', 'localhost')
REDIS_PORT = os.environ.get('APP_REDIS_PORT', '6379')
REDIS_DB = os.environ.get('APP_REDIS_DB', '0')
REDIS_PASSWORD = os.environ.get('APP_REDIS_PASS', '$1!GZp30OFu%tYXmzzLXN7Q9Ik$ZNsKL9a4GdSNxX')
BROKER_URL = 'redis://' + ':' + REDIS_PASSWORD + '@' + REDIS_HOST + ':' + REDIS_PORT + '/' + REDIS_DB
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600}
CELERY_RESULT_BACKEND = BROKER_URL
CELERY_IGNORE_RESULT = True

REDIS_POOL_DB = int(os.environ.get('APP_REDIS_POOL_DB', 1))
REDIS_POOL = redis.ConnectionPool(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_POOL_DB,
                                  password=REDIS_PASSWORD, decode_responses=True)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_TZ = True

STATIC_URL = 'static/'
STATIC_ROOT = BASE_DIR / 'static'
STATICFILES_DIRS = [
    'templates/static',
]

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

ORDER_BOOK_UPDATE_INTERVAL = int(os.environ.get('APP_ORDER_BOOK_UPDATE_INTERVAL_SECONDS', 10))  # seconds

ORDER_BOOK_STORE_TIME = int(os.environ.get('APP_ORDER_BOOK_STORE_TIME_MINUTES', 10))  # minutes
