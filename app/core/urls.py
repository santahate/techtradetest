from django.contrib import admin
from django.urls import path, include


urlpatterns = [
    path('', include(('order_book.urls', 'order_book')), name='order_book'),
    path('admin/', admin.site.urls),
]
