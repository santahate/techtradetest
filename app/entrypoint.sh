#!/bin/bash

# wait until MySQL is really available
until nc -z "$APP_MYSQL_HOST" 3306; do
  >&2 echo "MySQL is unavailable - sleeping"
  sleep 1
done

if [ "$1" = "app" ]
then
    python manage.py makemigrations || exit 1
    python manage.py migrate || exit 1
    python manage.py init_beat || exit 1
    gunicorn --bind :8080 core.wsgi:application --reload

elif [ "$1" = "beat" ]
then
    celery -A core beat -l INFO --scheduler django_celery_beat.schedulers:DatabaseScheduler
elif [ "$1" = "worker" ]
then
    celery -A core worker --loglevel=INFO
fi
