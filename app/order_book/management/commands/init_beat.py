from django_celery_beat.models import PeriodicTask, IntervalSchedule
from django.conf import settings
from django.core.management import BaseCommand


class Command(BaseCommand):
    """
    A management command that ensures periodic tasks for updating and clearing the order book are scheduled.
    """
    def handle(self, *args, **options):
        update_schedule, _ = IntervalSchedule.objects.get_or_create(
            every=settings.ORDER_BOOK_UPDATE_INTERVAL,
            period=IntervalSchedule.SECONDS,
        )

        clear_schedule, _ = IntervalSchedule.objects.get_or_create(
            every=settings.ORDER_BOOK_STORE_TIME,
            period=IntervalSchedule.MINUTES,
        )

        if not PeriodicTask.objects.filter(name='UpdateOrderBook').exists():
            PeriodicTask.objects.create(
                interval=update_schedule,
                name='UpdateOrderBook',
                task='order_book.tasks.UpdateOrderBook',
            )
        if not PeriodicTask.objects.filter(name='ClearOldOrderBook').exists():
            PeriodicTask.objects.create(
                interval=clear_schedule,
                name='ClearOldOrderBook',
                task='order_book.tasks.ClearOldOrderBook',
            )
