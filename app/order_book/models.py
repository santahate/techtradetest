from django.db import models


class OrderBook(models.Model):
    lastUpdateId = models.BigIntegerField(primary_key=True)
    symbol = models.CharField(max_length=255, db_index=True)
    asks = models.JSONField()
    bids = models.JSONField()

    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.symbol} {self.lastUpdateId}'

    class Meta:
        db_table = 'order_book'
