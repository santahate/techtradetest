import json

import redis
from django.conf import settings

from core.constants import ORDER_BOOK_CACHE_KEY
from order_book.models import OrderBook


redis_connect = redis.Redis(connection_pool=settings.REDIS_POOL)


def get_last_order_book() -> dict:
    # try to get data from cache
    cached_data = redis_connect.get(ORDER_BOOK_CACHE_KEY)
    if cached_data:
        return json.loads(cached_data)

    try:
        latest_order_book = OrderBook.objects.latest('created_at')
    except OrderBook.DoesNotExist:
        return {'asks': [], 'bids': [], 'last_updated': ''}

    # get data from db if no cache available
    asks = []
    for ask in latest_order_book.asks:
        asks.append([ask['price'], ask['quantity'], ask['price'] * ask['quantity']])
    bids = []
    for bid in latest_order_book.bids:
        bids.append([bid['price'], bid['quantity'], bid['price'] * bid['quantity']])
    last_updated = latest_order_book.created_at.strftime("%Y-%m-%d %H:%M:%S") + ' UTC'

    result = {'asks': asks, 'bids': bids, 'last_updated': last_updated}

    # set cache
    redis_connect.set(ORDER_BOOK_CACHE_KEY, json.dumps(result))
    redis_connect.expire(ORDER_BOOK_CACHE_KEY, settings.ORDER_BOOK_UPDATE_INTERVAL)

    return result
