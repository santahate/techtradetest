import datetime

from binance import Client
from django.db import IntegrityError

from .models import OrderBook


def update_orders(symbol: str = 'BTCUSDT', limit: int = 1000) -> None:
    """
    Get order book from binance and save it to database
    :param symbol: str
    :param limit: int
    """
    client = Client()
    order_book = client.get_order_book(symbol=symbol, limit=limit)

    asks = [{"price": float(ask[0]), "quantity": float(ask[1])} for ask in order_book['asks']]
    bids = [{"price": float(bid[0]), "quantity": float(bid[1])} for bid in order_book['bids']]

    try:
        OrderBook.objects.create(
            lastUpdateId=order_book['lastUpdateId'],
            symbol=symbol,
            asks=asks,
            bids=bids,
        )
    except IntegrityError:
        pass


def clear_old_order_book():
    """
    Remove OrderBook records older than 10 minutes
    :return:
    """
    OrderBook.objects\
        .filter(created_at__lte=datetime.datetime.utcnow() - datetime.timedelta(minutes=10))\
        .delete()
