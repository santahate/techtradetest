from celery import Task

from .services import update_orders, clear_old_order_book
from core.celery import app


class UpdateOrderBook(Task):
    def run(self):
        update_orders()
        return 'OK'


UpdateOrderBook = app.register_task(UpdateOrderBook())


class ClearOldOrderBook(Task):
    def run(self):
        clear_old_order_book()
        return 'OK'


ClearOldOrderBook = app.register_task(ClearOldOrderBook())
