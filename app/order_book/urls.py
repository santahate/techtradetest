from django.urls import path

from order_book.views import index, get_order_book

urlpatterns = [
    path('', index, name='index'),
    path('get_order_book/', get_order_book, name='get_order_book'),
]
