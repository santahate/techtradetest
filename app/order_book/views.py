from annoying.decorators import render_to
from django.http import JsonResponse

from order_book.selectors import get_last_order_book


@render_to('order_book/order_book.html')
def index(request):
    return {}


def get_order_book(request):
    data = get_last_order_book()
    return JsonResponse(data=data)
